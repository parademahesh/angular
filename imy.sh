while getopts h:r:w: option
 do
         case "${option}"
         in
                 r) restart=${OPTARG};;
                 w) ids=${OPTARG};;
                 h) mongohost=${OPTARG};;

         esac

done


currentDir=$(pwd);
mongoPath="$currentDir/bin";
mongohost=$mongohost;
mongoDb="ProcessStore"
ids=($ids)
restart=$restart;
CURRENT_DATE=`date +%Y-%m-%d.%H.%M.%S`;
mkdir $CURRENT_DATE
cd $CURRENT_DATE
mkdir "jsons"
mkdir "AppProcesses"
mkdir "DeployedProcessStack"
mkdir "Projects"
mkdir "DataTypes"
mkdir "AppDataTypes"
cd ../
rm -R jsons
tar -zxvf jsons.tar.gz

exportAsJSON() {

        cd ../
        local docId=${1};
        local collectionName=${2};
        echo "Came into function export As JSON - $docId - - $collectionName --- $(pwd)";

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({_id: \"$docId\"}), '', true)" > "$CURRENT_DATE/jsons/$docId.json"
        cd "jsons"
#       echo $exportCMD;
#       $exportCMD;

}


exportAsJSONApplication() {

        cd ../
        #echo "Came into function export As JSON";
        local docId=${1};
        rm "$CURRENT_DATE/AppProcesses/$docId.json";
        local collectionName=${2};
        echo "Came into function export As application JSON $docId  - $collectionName - -- $(pwd)";
        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "$CURRENT_DATE/AppProcesses/$docId.json"
        cd "AppProcesses"
#       echo $exportCMD;
#       $exportCMD;

}

exportAsJSONDeployedProcessStack() {

        cd ../
        #echo "Came into function export As JSON";
        local docId=${1};
        rm "$CURRENT_DATE/DeployedProcessStack/$docId.json";
        local collectionName=${2};
        echo "Came into function export As deployed process stack JSON $docId  - $collectionName";

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({_id: \"$docId\"}), '', true)" > "$CURRENT_DATE/DeployedProcessStack/$docId.json"
        cd "DeployedProcessStack"
#       echo $exportCMD;
#       $exportCMD;

}


exportAsJSONProjects() {

        cd ../
        #echo "Came into function export As JSON";
        local docId=${1};
        rm "$CURRENT_DATE/Projects/$docId.json";
        local collectionName=${2};
        echo "Came into function export As application JSON $docId  - $collectionName - -- $(pwd)";
        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "$CURRENT_DATE/Projects/$docId.json"
        cd "Projects"
#       echo $exportCMD;
#       $exportCMD;

}


exportAsJSONDataTypes() {

        cd ../
        #echo "Came into function export As JSON";
        local docId=${1};
        rm "$CURRENT_DATE/DataTypes/$docId.json";
        local collectionName=${2};
        echo "Came into function export As application JSON $docId  - $collectionName - -- $(pwd)";
        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "$CURRENT_DATE/DataTypes/$docId.json"
        cd "DataTypes"
#       echo $exportCMD;
#       $exportCMD;

}


exportAsJSONAppDataTypes() {

        cd ../
        #echo "Came into function export As JSON";
        local docId=${1};
        rm "$CURRENT_DATE/AppDataTypes/$docId.json";
        local collectionName=${2};
        echo "Came into function export As application JSON $docId  - $collectionName - -- $(pwd)";
        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "$CURRENT_DATE/AppDataTypes/$docId.json"
        cd "AppDataTypes"
#       echo $exportCMD;
#       $exportCMD;

}

removeWorkflow() {
        #echo "Came into remove workflow function"
        local docId=${1}
        local collectionName=${2};
        #echo "ID $docId  collection name $collectionName  mongo host $mongohost  --- $mongohost/$mongoDb";

        $mongoPath/mongo $mongohost/$mongoDb --eval "db.$collectionName.remove({_id:\"$docId\"})";

}

removeAppWorkflow() {
        #echo "Came into remove workflow function"
        local docId=${1}
        local collectionName=${2};
        #echo "ID $docId  collection name $collectionName  mongo host $mongohost  --- $mongohost/$mongoDb";

        $mongoPath/mongo $mongohost/$mongoDb --eval "db.$collectionName.remove({id:\"$docId\"})";

}


restoreWorkflow() {
        #echo "Came into restore workflow";
        local docId=${1};
        local collectionName=${2};

        #echo "ID $docId  collection name $collectionName  mongo path - $mongoPath   ";
        $mongoPath/mongoimport --host $mongohost --db $mongoDb --collection $collectionName < $docId;

}


restoreAppWorkflow() {
        #echo "Came into restore workflow";
        local docId=${1};
        local collectionName=${2};

        #echo "ID $docId  collection name $collectionName  mongo path - $mongoPath   ";
        $mongoPath/mongoimport --host $mongohost --db $mongoDb --collection $collectionName < $docId;

}

removeRestoreJsons () {
        cd jsons
        yourfilenames=`ls *.json`
        for eachfile in $yourfilenames
        do
           #echo "File name $eachfile";
           filename=$(basename -- "$eachfile")
           extension="${filename##*.}"

           id="${filename%.*}"
           #echo "remove file name $filename  id $id"
           exportAsJSON "$id" "ProcessStack"
           #exportAsJSONApplication "$id" "AppProcesses"
           #exportAsJSONDeployedProcessStack "$id" "DeployedProcessStack"
           echo "remove workflow - $id  - $(pwd)"
           removeWorkflow "$id" "ProcessStack"
           #removeWorkflow "$id" "DeployedProcessStack"
           echo "Restore in-progress - file name - $eachfile  - $(pwd)"
           restoreWorkflow "$eachfile" "ProcessStack"
           #restoreWorkflow "$eachfile" "DeployedProcessStack"
        done
        cd ../
}


removeRestoreDeployedProcessStack () {

        echo "current path - $(pwd)"
        cd "DeployedProcessStack"
        yourfilenames=`ls *.json`
        for eachfile in $yourfilenames
        do
           #echo "File name $eachfile";
           filename=$(basename -- "$eachfile")
           extension="${filename##*.}"

           id="${filename%.*}"
           #echo "remove file name $filename  id $id"
           #exportAsJSON "$id" "ProcessStack"
           #exportAsJSONApplication "$id" "AppProcesses"
           exportAsJSONDeployedProcessStack "$id" "DeployedProcessStack"

           echo "Restore Deployed processstack path - $(pwd)"
           #removeWorkflow "$id" "ProcessStack"
           removeWorkflow "$id" "DeployedProcessStack"
           #restoreWorkflow "$eachfile" "ProcessStack"
           restoreWorkflow "$eachfile" "DeployedProcessStack"
        done
        cd ../
}


removeRestoreAppProcess() {
        cd "AppProcesses"
        yourfilenames=`ls *.json`
        for eachfile in $yourfilenames
        do
           #echo "File name $eachfile";
           filename=$(basename -- "$eachfile")
           extension="${filename##*.}"

           id="${filename%.*}"
           #echo "remove file name $filename  id $id"
           #exportAsJSON "$id" "ProcessStack"
           #exportAsJSONApplication "$id" "AppProcesses"
           exportAsJSONApplication "$id" "AppProcesses"

           echo "Restore app process path - ($pwd)"
           #removeWorkflow "$id" "ProcessStack"
           removeAppWorkflow "$id" "AppProcesses"
           #restoreWorkflow "$eachfile" "ProcessStack"
           restoreWorkflow "$eachfile" "AppProcesses"

        done
        cd ../
}

removeRestoreProjects() {

        cd "Projects"
        yourfilenames=`ls *.json`
        for eachfile in $yourfilenames
        do
           #echo "File name $eachfile";
           filename=$(basename -- "$eachfile")
           extension="${filename##*.}"

           id="${filename%.*}"
           exportAsJSONProjects "$id" "Project"

           echo "Restore Project path - ($pwd)"
           removeWorkflow "$id" "Project"
           restoreWorkflow "$eachfile" "Project"

        done
        cd ../

}

removeRestoreDataTypes() {

        cd "DataTypes"
        yourfilenames=`ls *.json`
        for eachfile in $yourfilenames
        do
           #echo "File name $eachfile";
           filename=$(basename -- "$eachfile")
           extension="${filename##*.}"

           id="${filename%.*}"
           exportAsJSONDataTypes "$id" "Datatypes"

           echo "Restore Datatype path - ($pwd)"
           removeAppWorkflow "$id" "Datatypes"
           restoreWorkflow "$eachfile" "Datatypes"

        done
        cd ../

}


removeRestoreAppDataTypes() {

        cd "AppDataTypes"
        yourfilenames=`ls *.json`
        for eachfile in $yourfilenames
        do
           #echo "File name $eachfile";
           filename=$(basename -- "$eachfile")
           extension="${filename##*.}"

           id="${filename%.*}"
           exportAsJSONAppDataTypes "$id" "AppDatatypes"

           echo "Restore App Datatypes path - ($pwd)"
           removeAppWorkflow "$id" "AppDatatypes"
           restoreWorkflow "$eachfile" "AppDatatypes"

        done
        cd ../

}


removeRestoreJsons;
removeRestoreDeployedProcessStack;
removeRestoreAppProcess;
removeRestoreProjects;
removeRestoreDataTypes;
removeRestoreAppDataTypes;

echo "current path $(pwd)"
rm -R jsons
rm -R DeployedProcessStack
rm -R AppProcesses
rm -R Projects
rm -R DataTypes
rm -R AppDataTypes
