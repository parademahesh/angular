while getopts h:r:w:p:d: option
 do
         case "${option}"
         in
                 r) restart=${OPTARG};;
                 w) ids=${OPTARG};;
                 h) mongohost=${OPTARG};;
                 p) projectIds=${OPTARG};;
                 d) dataTypes=${OPTARG};;

         esac

done


currentDir=$(pwd);
mongoPath="$currentDir/bin";
mongohost=$mongohost;
mongoDb="ProcessStore"
ids=($ids)
projectIds=($projectIds)
dataTypes=($dataTypes)
restart=$restart;

if [ ! -z "$mongohost" -a "$mongohost" != " " ]; then
        echo "Mongo Host PORT - "$mongohost
else
  echo "Mongo HOST PORT is mandatory";
  exit;
fi


if [ ${#projectIds[@]} -eq 0 ]; then
    echo "Project ID is mandatory";
    exit;
fi

if [ ${#ids[@]} -eq 0 ]; then
    echo "Workflow ID is mandatory";
    exit;
fi

if [ ${#ids[@]} -gt 1 ]; then
    echo "Maximum one workflow allowed to deploy";
    #exit;
fi

echo "Length of the workflow array = ${#ids[@]}";


rm -R jsons
rm -R DeployedProcessStack
rm -R AppProcesses
rm -R Projects
rm -R DataTypes
rm -R AppDataTypes
mkdir jsons
mkdir DeployedProcessStack
mkdir AppProcesses
mkdir Projects
mkdir DataTypes
mkdir AppDataTypes

exportAsJSON() {

        #echo "Came into function export As JSON";
        local docId=${1};
        rm "jsons/$docId.json";
        local collectionName=${2};

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({_id: \"$docId\"}), '', true)" > "jsons/$docId.json"
#       echo $exportCMD;
#       $exportCMD;

}

exportAsJSONApplication() {

        #echo "Came into function export As JSON";
        local docId=${1};
        rm "AppProcesses/$docId.json";
        local collectionName=${2};
        echo "Came into function export As application JSON $docId  - $collectionName";
        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "AppProcesses/$docId.json"
#       echo $exportCMD;
#       $exportCMD;

}

exportAsJSONDeployedProcessStack() {

        #echo "Came into function export As JSON";
        local docId=${1};
        rm "DeployedProcessStack/$docId.json";
        local collectionName=${2};
        echo "Came into function export As deployed process stack JSON $docId  - $collectionName";

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({_id: \"$docId\"}), '', true)" > "DeployedProcessStack/$docId.json"
#       echo $exportCMD;
#       $exportCMD;

}

exportAsJSONProject() {

        echo "Came into function export project";
        local docId=${1};
        rm "Projects/$docId.json";
        local collectionName=${2};
        echo "Came into function export As JSON PROJECT $docId  - $collectionName";

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({_id: \"$docId\"}), '', true)" > "Projects/$docId.json"
#       echo $exportCMD;
#       $exportCMD;

}



exportAsJSONDataTypes() {

        echo "Came into function export project";
        local docId=${1};
        rm "DataTypes/$docId.json";
        local collectionName=${2};
        echo "Came into function export As JSON DataTypes $docId  - $collectionName";

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "DataTypes/$docId.json"
#       echo $exportCMD;
#       $exportCMD;

}

exportAsJSONAppDataTypes() {

        local docId=${1};
        rm "AppDataTypes/$docId.json";
        local collectionName=${2};
        echo "Came into function export As JSON App DataTypes $docId  - $collectionName";

        $mongoPath/mongo $mongohost/$mongoDb --quiet --eval "tojson(db.$collectionName.findOne({id: \"$docId\"}), '', true)" > "AppDataTypes/$docId.json"
#       echo $exportCMD;
#       $exportCMD;

}


removeWorkflow() {
        #echo "Came into remove workflow function"
        local docId=${1}
        local collectionName=${2};
        #echo "ID $docId  collection name $collectionName  mongo host $mongohost  --- $mongohost/$mongoDb";

        $mongoPath/mongo $mongohost/$mongoDb --eval "db.$collectionName.remove({_id:\"$docId\"})";

}


restoreWorkflow() {
        #echo "Came into restore workflow";
        local docId=${1};
        local collectionName=${2};

        #echo "ID $docId  collection name $collectionName  mongo path - $mongoPath   ";
        $mongoPath/mongoimport --host $mongohost --db $mongoDb --collection $collectionName < $docId;

}

for anID in ${ids[@]}
        do
                exportAsJSON "$anID" "ProcessStack"
                exportAsJSONApplication "$anID" "AppProcesses"
                exportAsJSONDeployedProcessStack "$anID" "DeployedProcessStack"

        done

for anID in ${projectIds[@]}
        do
                echo "an ID inside for loop $anID"
                exportAsJSONProject "$anID" "Project"
        done

for anID in ${dataTypes[@]}
        do
                echo "an ID inside for loop $anID"
                exportAsJSONDataTypes "$anID" "Datatypes"
                exportAsJSONAppDataTypes "$anID" "AppDatatypes"
        done


rm jsons.tar.gz
tar -zcvf jsons.tar.gz jsons DeployedProcessStack AppProcesses Projects DataTypes AppDataTypes
rm -R jsons AppProcesses DeployedProcessStack Projects DataTypes AppDataTypes

